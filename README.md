# View Generator
Technology Used: Django

### Commands To fire up Django Server 
1. .\virenv\Scripts\activate.bat
2. cd ViewGenerator
3. python manage.py runserver

### Why it is required?
1. To send the schema JSON to ReactJS.

### Reference
1. https://docs.djangoproject.com/en/1.8/intro/tutorial01/


--- 

# ViewGeneratorUI
Technology Used: ReactJS

### Commands to fire up ReactJS
1. cd viewgeneratorui
2. npm start

### Why it is required?
1. To get the schema JSON to ReactJS.
2. Prepare the GUI for the server to preapre Object with validations.

### Reference
1. https://reactjs.org/docs/create-a-new-react-app.html

--- 

### Type of Associations in Class:
1. Association
2. Aggregation
3. Composition
4. Inheritance

### Type of Associations in Table:
1. One to One
2. One to Many
3. Many to Many

--- 

